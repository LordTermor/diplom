using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Reactive.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Controls.Templates;
using Avalonia.Data;
using Avalonia.Markup.Xaml.Templates;
using DiplomModel;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Диалог добавления нового элемента
    /// </summary>
    /// <typeparam name="T">Тип элемента</typeparam>
    public partial class SqlAddDialog<T> : Window where T : EntityObject, new()
    {
        private DbContext _repository = new(new DiplomEntities(), true);
        private readonly Button _addButton = new() {Content = "Добавить"};

        private readonly Dictionary<Type, Func<SqlProperty, Control>> _handlers = new()
        {
            {
                typeof(bool),
                property => new CheckBox {[!ToggleButton.IsCheckedProperty] = new Binding(property.BindingName)}
            },
            {
                typeof(string),
                property => new TextBox {[!TextBox.TextProperty] = new Binding(property.BindingName)}
            },
            {
                typeof(int),
                property => new NumericUpDown {[!NumericUpDown.ValueProperty] = new Binding(property.BindingName)}
            },
            {
                typeof(DateTime),
                property => new DatePicker
                {
                    [!DatePicker.SelectedDateProperty] =
                        new Binding(property.BindingName, BindingMode.TwoWay)
                            {Converter = new DateTimeOffsetValueConverter()}
                }
            }
        };


        /// <summary>
        /// Получение представления выпадающего меню для внешнего ключа
        /// </summary>
        /// <param name="prop">Столбец таблицы внешнего ключа</param>
        /// <returns>Представление выпадающего меню</returns>
        private Control GetHandler(SqlProperty prop)
        {
            if (prop.Reference == null)
            {
                var handler = _handlers[prop.Type](prop);
                handler.DataContext = _toAdd;
                return handler;
            }

            _repository.Set(prop.Reference.Value.Type).Load();
            var itms = _repository.Set(prop.Reference.Value.Type).Local;
            return new ComboBox()
            {
                DataContext = _toAdd,
                Items = itms,
                ItemTemplate = DisplayTemplates.GetTemplate(prop.Reference.Value.Type),
                [!SelectingItemsControl.SelectedItemProperty] = new Binding(prop.BindingName, BindingMode.TwoWay)
            };
        }


        private IEnumerable<SqlProperty> Props { get; }
        private readonly T _toAdd = new();

        public SqlAddDialog(IEnumerable<SqlProperty> props)
        {
            Props = props;
            DataContext = _toAdd;
            SizeToContent = SizeToContent.Height;
            MaxWidth = 400;
            CanResize = false;

            Content = new StackPanel();
            var content = (StackPanel) Content;

            _addButton.Command = ReactiveCommand.Create(_add);


            foreach (var property in Props)
                content.Children.Add(new StackPanel
                {
                    Children =
                    {
                        new Label {Content = $"{property.DisplayName}"},
                        GetHandler(property)
                    }
                });

            content.Children.Add(_addButton);
        }


        private void _add()
        {
            Close(_toAdd);
        }
    }
}