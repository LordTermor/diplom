using System.Reactive.Disposables;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Controls.Templates;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Представление перевода из класса в класс
    /// </summary>
    public class Transfer : ReactiveUserControl<TransferViewModel>
    {
        public Transfer()
        {
            Tag = "Перевод";
            ViewModel = new TransferViewModel();
            InitializeComponent();


            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel,
                        vm => vm.Classes,
                        view => view.LeftClassesComboBox.Items)
                    .DisposeWith(disposable);
                ;
                this.OneWayBind(ViewModel,
                        vm => vm.Classes,
                        view => view.RightClassesComboBox.Items)
                    .DisposeWith(disposable);
                ;

                this.OneWayBind(ViewModel,
                        vm => vm.LeftStudents,
                        view => view.LeftStudentsListBox.Items)
                    .DisposeWith(disposable);
                ;

                this.OneWayBind(ViewModel,
                        vm => vm.RightStudents,
                        view => view.RightStudentsListBox.Items)
                    .DisposeWith(disposable);
                ;

                this.Bind(ViewModel,
                        vm => vm.SelectedClassLeft,
                        view => view.LeftClassesComboBox.SelectedItem)
                    .DisposeWith(disposable);
                ;

                this.Bind(ViewModel,
                        vm => vm.SelectedClassRight,
                        view => view.RightClassesComboBox.SelectedItem)
                    .DisposeWith(disposable);
                ;

                this.BindCommand(ViewModel,
                        model => model.ToRight,
                        wizard => wizard.RightButton,
                        LeftStudentsListBox.GetObservable(SelectingItemsControl.SelectedItemProperty))
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.ToLeft,
                        wizard => wizard.LeftButton,
                        RightStudentsListBox.GetObservable(SelectingItemsControl.SelectedItemProperty))
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.Save,
                        view => view.SaveButton)
                    .DisposeWith(disposable);
            });
        }

        private ComboBox LeftClassesComboBox => this.FindControl<ComboBox>("LeftClassesComboBox");
        private ComboBox RightClassesComboBox => this.FindControl<ComboBox>("RightClassesComboBox");
        private ListBox LeftStudentsListBox => this.FindControl<ListBox>("LeftStudentsListBox");
        private ListBox RightStudentsListBox => this.FindControl<ListBox>("RightStudentsListBox");
        private Button LeftButton => this.FindControl<Button>("LeftButton");
        private Button RightButton => this.FindControl<Button>("RightButton");
        private Button SaveButton => this.FindControl<Button>("SaveButton");


        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}