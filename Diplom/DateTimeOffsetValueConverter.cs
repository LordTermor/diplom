using System;
using System.Globalization;
using Avalonia.Data.Converters;

namespace Diplom
{
    /// <summary>
    /// Класс-конвертер значений даты/времени
    /// </summary>
    public class DateTimeOffsetValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType.IsAssignableFrom(typeof(DateTimeOffset)) && value is DateTime dateTime)
                return new DateTimeOffset(dateTime);

            return null;
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType.IsAssignableFrom(typeof(DateTime)) && value is DateTimeOffset offset)
                return offset.UtcDateTime;

            return null;
        }
    }
}