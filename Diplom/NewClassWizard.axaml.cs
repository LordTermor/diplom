using System.Reactive.Disposables;
using System.Reactive.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Представление добавления нового класса
    /// </summary>
    public class NewClassWizard : ReactiveUserControl<NewClassViewModel>
    {
        private Button AddButton => this.FindControl<Button>("AddButton");
        private ListBox AllTeachersListBox => this.FindControl<ListBox>("AllTeachersListBox");
        private Button LeftButton => this.FindControl<Button>("LeftButton");
        private Button RightButton => this.FindControl<Button>("RightButton");
        private ListBox SelectedTeachersListBox => this.FindControl<ListBox>("SelectedTeachersListBox");
        private StackPanel ClassToAddPropsWrapPanel => this.FindControl<StackPanel>("ClassToAddPropsWrapPanel");
        private ComboBox AvailableTeachersCombobox => this.FindControl<ComboBox>("AvailableTeachersCombobox");
        private ComboBox AvailableYearsComboBox => this.FindControl<ComboBox>("AvailableYearsComboBox");

        public NewClassWizard()
        {
            Tag = "Создать класс";
            ViewModel = new NewClassViewModel();


            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel,
                        model => model.UnselectedStudents,
                        view => view.AllTeachersListBox.Items)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel,
                        model => model.SelectedStudents,
                        wizard => wizard.SelectedTeachersListBox.Items)
                    .DisposeWith(disposable);


                this.BindCommand(ViewModel,
                        model => model.Select,
                        wizard => wizard.RightButton,
                        AllTeachersListBox.GetObservable(SelectingItemsControl.SelectedItemProperty))
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.Deselect,
                        wizard => wizard.LeftButton,
                        SelectedTeachersListBox.GetObservable(SelectingItemsControl.SelectedItemProperty))
                    .DisposeWith(disposable);


                this.OneWayBind(ViewModel,
                        model => model.ClassToAdd,
                        wizard => wizard.ClassToAddPropsWrapPanel.DataContext)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.Add,
                        wizard => wizard.AddButton)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel,
                        model => model.Teachers,
                        wizard => wizard.AvailableTeachersCombobox.Items)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel,
                        model => model.AvailableYears,
                        wizard => wizard.AvailableYearsComboBox.Items)
                    .DisposeWith(disposable);
            });

            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}