using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using DiplomModel;
using DynamicData;
using DynamicData.Binding;
using ReactiveUI;

namespace Diplom
{
    public class ClassParallelGradeBook : ReactiveUserControl<ClassParallelGradeBookViewModel>
    {
        [Flags]
        public enum Filters
        {
            No,
            Student
        }

        public ClassParallelGradeBook()
        {
        }

        public ClassParallelGradeBook(string name = "Журнал успеваемости")
        {
            Tag = name;

            ViewModel = new ClassParallelGradeBookViewModel();


            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel,
                        x => x.ClassParallels,
                        y => y.ParallelComboBox.Items)
                    .DisposeWith(disposable);

                this.Bind(ViewModel,
                        x => x.CurrentParallel,
                        y => y.ParallelComboBox.SelectedItem)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel,
                        model => model.Rows,
                        view => view.GradebookDataGrid.Items)
                    .DisposeWith(disposable);
            });

            InitializeComponent();
        }

        private ComboBox ParallelComboBox => this.FindControl<ComboBox>("ParallelComboBox");

        private DataGrid GradebookDataGrid => this.FindControl<DataGrid>("GradebookDataGrid");

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}