using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using DiplomModel;
using DynamicData.Binding;
using DynamicData.Kernel;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Модель представления успеваемости
    /// </summary>
    public class GradeBookViewModel : ReactiveObject, IActivatableViewModel
    {
        private readonly DbContext _repository = new(new DiplomEntities(), true);
        private Класс _currentClass;
        private Предмет _currentSubject;
        private DateTimeOffset _dateFilterFrom;
        private DateTimeOffset _dateFilterTo;
        private bool _filterEnabled;
        private Ученик _student;

        public GradeBookViewModel()
        {
            Classes = new ObservableCollectionExtended<Класс>(_repository.Set<Класс>().ToArray());
            Subjects = new ObservableCollectionExtended<Предмет>(_repository.Set<Предмет>().ToArray());
            _dateFilterFrom = DateTimeOffset.Now;
            _dateFilterTo = DateTimeOffset.Now;

            AddCommand = ReactiveCommand.Create<DateTimeOffset>(_add);
            SaveCommand = ReactiveCommand.Create(_save);


            this.WhenActivated(disposable =>
            {
                this.WhenAnyValue(
                        x => x.CurrentSubject,
                        x => x.DateFilterFrom,
                        x => x.DateFilterTo,
                        x => x.FilterEnabled,
                        x => x.Student).Subscribe(_ => update())
                    .DisposeWith(disposable);

                this.WhenAnyValue(x => x.CurrentClass)
                    .Subscribe(x =>
                    {
                        if (x == null) return;
                        Student = null;
                        AvailableStudents.Load(x.Ученикs.ToList());
                        update();
                    }).DisposeWith(disposable);

                update();
                Disposable
                    .Create(() => { })
                    .DisposeWith(disposable);
            });
        }

        public ObservableCollectionExtended<Класс> Classes { get; set; }

        public Класс CurrentClass
        {
            get => _currentClass;
            set => this.RaiseAndSetIfChanged(ref _currentClass, value);
        }

        public ObservableCollectionExtended<Предмет> Subjects { get; set; }

        public ObservableCollectionExtended<Ученик> AvailableStudents { get; set; } = new();

        public Ученик Student
        {
            get => _student;
            set => this.RaiseAndSetIfChanged(ref _student, value);
        }

        public Предмет CurrentSubject
        {
            get => _currentSubject;
            set => this.RaiseAndSetIfChanged(ref _currentSubject, value);
        }

        public ObservableCollection<Журнал> Grades { get; set; }

        public ObservableCollectionExtended<SqlProperty> Columns { get; set; } = new();
        public ObservableCollectionExtended<Row> Rows { get; set; } = new();

        public ReactiveCommand<DateTimeOffset, Unit> AddCommand { get; set; }
        public ReactiveCommand<Unit, Unit> SaveCommand { get; set; }

        public ReactiveCommand<KeyValuePair<int, string>, Unit> RemoveCommand =>
            ReactiveCommand.Create<KeyValuePair<int, string>>(_remove);

        public DateTimeOffset DateFilterFrom
        {
            get => _dateFilterFrom;
            set => this.RaiseAndSetIfChanged(ref _dateFilterFrom, value);
        }

        public DateTimeOffset DateFilterTo
        {
            get => _dateFilterTo;
            set => this.RaiseAndSetIfChanged(ref _dateFilterTo, value);
        }

        public bool FilterEnabled
        {
            get => _filterEnabled;
            set => this.RaiseAndSetIfChanged(ref _filterEnabled, value);
        }

        public ViewModelActivator Activator { get; } = new();

        /// <summary>
        /// Удаление значение оценки
        /// </summary>
        /// <param name="idDatePair">Пара идентификатор ученика-дата</param>
        private async void _remove(KeyValuePair<int, string> idDatePair)
        {
            try
            {
                var grade = Grades.First(grade => grade.Дата.ToShortDateString() == idDatePair.Value
                                                  && grade.Предмет == _currentSubject &&
                                                  grade.IdУченика == idDatePair.Key);
                _repository.Set<Журнал>().Remove(grade);
                await _repository.SaveChangesAsync();
                update();
            }
            catch (Exception e)
            {
                var tmpE = e;
                while (tmpE.InnerException != null) tmpE = tmpE.InnerException;
                Utils.ShowError(tmpE.Message);
            }
        }

        /// <summary>
        /// Сохранение данных в базе
        /// </summary>
        private void _save()
        {
            try
            {
                foreach (var row in Rows)
                foreach (var rowMark in row.Marks)
                {
                    var marks = rowMark.Value.Split('/');

                    foreach (var mark in marks)
                    {
                        var added = _repository.Set<Журнал>().Create();
                        added.IdУченика = row.Id;
                        added.Дата = DateTime.Parse(rowMark.Key);
                        added.Оценка = mark;
                        added.Посещаемость = "+";
                        added.Предмет = CurrentSubject;
                        added.IdСотрудника = 1;
                    }
                }

                _repository.SaveChanges();
            }
            catch (Exception e)
            {
                var tmpE = e;
                while (tmpE.InnerException != null) tmpE = tmpE.InnerException;
                Utils.ShowError(tmpE.Message);
            }
        }

        /// <summary>
        /// Добавление даты
        /// </summary>
        /// <param name="date"></param>
        private void _add(DateTimeOffset date)
        {
            var g = date.Date;
            Columns.Add(new SqlProperty()
            {
                DisplayName = g.ToShortDateString(),
                BindingName = "Marks[" + g.ToShortDateString() + "]",
                Type = typeof(IEnumerable<string>)
            });
        }

        private struct GradeMark
        {
            public string Оценка { get; set; }
            public DateTime Дата { get; set; }
        }

        /// <summary>
        /// Обновление таблицы
        /// </summary>
        private void update()
        {
            if (_currentClass == null || _currentSubject == null) return;

            var set = _repository.Set<Журнал>();
            set.Load();
            Grades = set.Local;

            if (DateFilterFrom.Date == DateTime.MinValue || DateFilterTo.Date == DateTime.MinValue)
            {
                DateFilterFrom = DateTimeOffset.Now;
                DateFilterTo = DateTimeOffset.Now;
            }


            var classStudents = _currentClass.Ученикs.ToList();
            var dates = classStudents.SelectMany(student =>
                student.Журналs.Where(gr => gr.IdПредмета == _currentSubject.IdПредмета).Select(g => g.Дата)).ToList();

            if (Student == null)
                Rows.Load(classStudents.Select(student => Row.FromStudent(student, dates, _currentSubject)).ToList());
            else
                Rows.Load(new[] {Row.FromStudent(Student, dates, _currentSubject)});

            dates = FilterEnabled
                ? dates.Where(date => date.InclusiveBetween(DateFilterFrom.Date, DateFilterTo.Date)).ToList()
                : new List<DateTime>() {DateTime.Today};

            Columns.Load(dates
                .Distinct()
                .Select(g => new SqlProperty
                {
                    DisplayName = g.ToShortDateString(),
                    BindingName = "Marks[" + g.ToShortDateString() + "]",
                    Type = typeof(IEnumerable<string>)
                }).ToList());


            Columns.Insert(0, new SqlProperty
            {
                DisplayName = "Имя",
                Type = typeof(string),
                BindingName = "Name"
            });
        }

        public struct Row
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Dictionary<string, string> Marks { get; set; }

            /// <summary>
            /// Создание строки таблицы из ученика
            /// </summary>
            /// <param name="student">Ученик</param>
            /// <param name="dates">Даты для фильтра</param>
            /// <param name="currentSubject">Текущий предмет</param>
            /// <returns></returns>
            public static Row FromStudent(Ученик student, IEnumerable<DateTime> dates, Предмет currentSubject)
            {
                var row = new Row
                {
                    Id = student.IdУченика,
                    Name = $"{student.Фамилия} {student.Имя} {student.Отчество}",
                    Marks = dates.SelectMany(date =>
                        {
                            var els = student.Журналs.Where(grade => grade.Дата == date &&
                                                                     grade.IdПредмета == currentSubject.IdПредмета)
                                .Select(grade => new GradeMark() {Оценка = grade.Оценка, Дата = grade.Дата});
                            var enumerable = els.ToArray();
                            if (enumerable.Any()) return enumerable;

                            return new GradeMark[] {new() {Дата = date}};
                        }).GroupBy(g => g.Дата)
                        .ToDictionary(grouping => grouping.Key.ToShortDateString(),
                            grouping => string.Join("/", grouping
                                .Select(g => g.Оценка)))
                };
                return row;
            }
        }
    }
}