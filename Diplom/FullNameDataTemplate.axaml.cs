using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace Diplom
{
    public partial class FullNameDataTemplate : UserControl
    {
        public FullNameDataTemplate()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}