using System;
using System.Reactive.Disposables;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Представление формы авторизации
    /// </summary>
    public class AuthControl : ReactiveUserControl<AuthViewModel>
    {
        private Button AuthButton => this.FindControl<Button>("authBtn");

        public AuthControl()
        {
            ViewModel = new AuthViewModel();

            DataContext = ViewModel;


            this.WhenActivated(disposable =>
            {
                this.BindCommand(ViewModel,
                    model => model.Auth,
                    window => window.AuthButton
                ).DisposeWith(disposable);


                ViewModel.Auth.Subscribe(value =>
                {
                    if (value.HasValue) App.CurrentRole.OnNext(value.Value);
                }).DisposeWith(disposable);
            });

            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}