namespace Diplom
{
    public enum Role
    {
        Secretary,
        FormMaster,
        Chief,
        Teacher,
        LoggedOut,
        Admin
    }
}