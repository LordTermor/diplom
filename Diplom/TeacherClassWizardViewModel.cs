using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using DiplomModel;
using DynamicData.Binding;
using JetBrains.Annotations;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Модель представления добавления учителя к классу
    /// </summary>
    public class TeacherClassWizardViewModel : ReactiveObject, IActivatableViewModel
    {
        private readonly DbContext _repository = new(new DiplomEntities(), true);
        private Класс _selectedClass = new();

        public TeacherClassWizardViewModel()
        {
            Classes = _repository.Set<Класс>().ToList();

            Teachers = _repository.Set<Сотрудник>().Where(ent => ent.Должность.IdДолжности == 3).ToList();

            SelectedTeachers = new ObservableCollectionExtended<Сотрудник>();
            UnselectedTeachers = new ObservableCollectionExtended<Сотрудник>();


            Select = ReactiveCommand.Create<Сотрудник>(teacher =>
            {
                if (UnselectedTeachers.Remove(teacher)) SelectedTeachers.Add(teacher);
            });
            Deselect = ReactiveCommand.Create<Сотрудник>(teacher =>
            {
                if (SelectedTeachers.Remove(teacher)) UnselectedTeachers.Add(teacher);
            });

            Save = ReactiveCommand.Create(_save);

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(x => x.SelectedClass).Subscribe(cls =>
                {
                    if (cls == null || cls.EntityState == EntityState.Detached) return;

                    var selected = cls.Сотрудникs;
                    selected.Load();

                    SelectedTeachers.Load(selected.ToList());
                    UnselectedTeachers.Load(
                        Teachers.Where(teacher => selected.All(s => s.IdСотрудника != teacher.IdСотрудника)).ToList()
                    );
                }).DisposeWith(disposables);

                /* handle activation */
                Disposable
                    .Create(() =>
                    {
                        /* handle deactivation */
                    })
                    .DisposeWith(disposables);
            });
        }

        public IEnumerable<Класс> Classes { get; set; }
        public ObservableCollectionExtended<Сотрудник> UnselectedTeachers { get; set; }
        public ObservableCollectionExtended<Сотрудник> SelectedTeachers { get; set; }

        public IEnumerable<Сотрудник> Teachers { get; set; }

        public ReactiveCommand<Сотрудник, Unit> Select { get; set; }
        public ReactiveCommand<Сотрудник, Unit> Deselect { get; set; }
        public ReactiveCommand<Unit, Unit> Save { get; set; }


        public Класс SelectedClass
        {
            get => _selectedClass;
            [UsedImplicitly] set => this.RaiseAndSetIfChanged(ref _selectedClass, value);
        }

        public ViewModelActivator Activator { get; } = new();

        /// <summary>
        /// Сохранение изменений
        /// </summary>
        private async void _save()
        {
            foreach (var selectedTeacher in SelectedTeachers) selectedTeacher.Классs1.Add(_selectedClass);
            await _repository.SaveChangesAsync();
        }
    }
}