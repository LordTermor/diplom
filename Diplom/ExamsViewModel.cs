using System;
using System.Data.Entity;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using DiplomModel;
using DynamicData.Binding;
using JetBrains.Annotations;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Модель представления экзаменов
    /// </summary>
    public class ExamsViewModel : ReactiveObject, IActivatableViewModel
    {
        private readonly DbContext _repository = new(new DiplomEntities(), true);
        private Класс _currentClass;
        private Ученик _currentStudent;
        private Предмет _currentSubject;

        public ExamsViewModel()
        {
            Save = ReactiveCommand.Create(_save);

            this.WhenActivated(disposable =>
            {
                this.WhenAnyValue(model => model.CurrentClass)
                    .Subscribe(cls =>
                    {
                        if (cls == null || cls.EntityState == EntityState.Detached) return;
                        Students.Load(cls.Ученикs.ToList());
                    });
                this.WhenAnyValue(model => model.CurrentStudent)
                    .Subscribe(stud =>
                    {
                        if (stud == null || stud.EntityState == EntityState.Detached) return;

                        Results.Load(stud.РезультатыОгэs.ToList());
                    });

                this.WhenAnyValue(model => model.CurrentSubject)
                    .Subscribe(subj =>
                    {
                        if (subj == null || subj.EntityState == EntityState.Detached) return;


                        Results.Load(subj.РезультатыОгэs.ToList());
                    });

                Classes.Load(_repository.Set<Класс>().ToList());
                Subjects.Load(_repository.Set<Предмет>().ToList());


                Disposable
                    .Create(() => { })
                    .DisposeWith(disposable);
            });
        }

        public ObservableCollectionExtended<Класс> Classes { get; set; } = new();
        public ObservableCollectionExtended<Ученик> Students { get; set; } = new();

        public ObservableCollectionExtended<Предмет> Subjects { get; set; } = new();
        public ObservableCollectionExtended<РезультатыОгэ> Results { get; set; } = new();

        public Класс CurrentClass
        {
            get => _currentClass;
            [UsedImplicitly] set => this.RaiseAndSetIfChanged(ref _currentClass, value);
        }

        public Ученик CurrentStudent
        {
            get => _currentStudent;
            [UsedImplicitly] set => this.RaiseAndSetIfChanged(ref _currentStudent, value);
        }

        public Предмет CurrentSubject
        {
            get => _currentSubject;
            [UsedImplicitly] set => this.RaiseAndSetIfChanged(ref _currentSubject, value);
        }


        public ReactiveCommand<Unit, Unit> Save { get; }
        public ReactiveCommand<РезультатыОгэ, Unit> Add => ReactiveCommand.Create<РезультатыОгэ>(_add);

        public ReactiveCommand<int, Unit> Remove =>
            ReactiveCommand.Create<int>(i =>
            {
                _repository.Set<РезультатыОгэ>().Remove(Results[i]);
                _repository.SaveChanges();
                Results.Load(CurrentStudent.РезультатыОгэs.ToList());
            });


        public ViewModelActivator Activator { get; } = new();

        /// <summary>
        /// Сохранение значений
        /// </summary>
        private async void _save()
        {
            try
            {
                await _repository.SaveChangesAsync();
            }
            catch (Exception e)
            {
                var tmpE = e;
                while (tmpE.InnerException != null) tmpE = tmpE.InnerException;
                Utils.ShowError("ОШИБКА: " + tmpE.Message);
            }
        }

        private async void _add(РезультатыОгэ results)
        {
            try
            {
                var toAdd = new РезультатыОгэ
                {
                    IdПредмета = results.IdПредмета,
                    IdУченика = results.IdУченика,
                    РезультатЕГЭ = results.РезультатЕГЭ,
                    РезультатОГЭ = results.РезультатОГЭ
                };

                var allres = _repository.Set<РезультатыОгэ>();

                var latestKey = allres.Max(x => x.IdРезультата);

                toAdd.IdРезультата = latestKey + 1;

                allres.Add(toAdd);

                await _repository.SaveChangesAsync();

                Results.Load(CurrentStudent.РезультатыОгэs.ToList());
            }
            catch (Exception e)
            {
                var tmpE = e;
                while (tmpE.InnerException != null) tmpE = tmpE.InnerException;
                Utils.ShowError("ОШИБКА: " + tmpE.Message);
            }
        }
    }
}