﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Subjects;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Controls.Templates;
using Avalonia.Data;
using Diplom;
using DiplomModel;
using DynamicData.Binding;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Модель представления таблицы справочника
    /// </summary>
    /// <typeparam name="T">Тип справочника</typeparam>
    public class SqlViewModel<T> : ReactiveObject, IActivatableViewModel where T : EntityObject, new()
    {
        private DbContext _repository;

        public readonly Subject<string> Error = new();

        private readonly EdmProperty key;

        public SqlViewModel()
        {
            _repository = new DbContext(new DiplomEntities(), true);


            Add = ReactiveCommand.Create<T>(_add);
            Delete = ReactiveCommand.Create<int>(_delete);
            Save = ReactiveCommand.CreateFromTask(async () =>
            {
                try
                {
                    await _repository.SaveChangesAsync();
                    Error.OnNext(null);
                }
                catch (Exception e)
                {
                    var tmpException = e;
                    while (tmpException.InnerException != null) tmpException = tmpException.InnerException;
                    Error.OnNext($"Ошибка SQL: {tmpException.Message}");
                }
            });

            _repository.Set<T>();

            var oc = ((IObjectContextAdapter) _repository).ObjectContext;
            key = oc.CreateObjectSet<T>().EntitySet.ElementType.KeyProperties.First();
            Properties = Utils.ExtractColumns(oc, typeof(T))
                .Where(property => !key.Equals(property))
                .Select(SqlProperty.FromEdmProperty).ToList();

            Properties.SortOverrides();
            var fks = Utils.GetNavigationProperties<T>(oc);

            Properties = Properties.Select(x =>
            {
                foreach (var fk in fks)
                {
                    if (!fk.GetDependentProperties().Any() ||
                        x.BindingName != fk.GetDependentProperties().First().Name) continue;

                    x.Reference = new ReferencedProperty()
                    {
                        BindingName = fk.TypeUsage.EdmType.Name,
                        DisplayName = Utils.DisplayCamelCaseString(fk.TypeUsage.EdmType.Name),
                        Type = Type.GetType(fk.TypeUsage.EdmType.FullName),
                    };
                    break;
                }

                return x;
            }).ToList();


            this.WhenActivated(disposables =>
            {
                Entities.Load(_repository.Set<T>().ToList());

                /* Handle activation */
                Disposable
                    .Create(() => { Entities.Clear(); })
                    .DisposeWith(disposables);
            });
        }

        public List<SqlProperty> Properties { get; set; }


        public ObservableCollectionExtended<T> Entities { get; } = new();

        public ReactiveCommand<T, Unit> Add { get; }

        public ReactiveCommand<int, Unit> Delete { get; }

        public ReactiveCommand<Unit, Unit> Save { get; }

        public ReactiveCommand<Unit, Unit> Refresh => ReactiveCommand.Create(() =>
        {
            _repository.Dispose();
            _repository = new DbContext(new DiplomEntities(), true);
            Entities.Clear();
            Entities.Load(_repository.Set<T>().ToList());
        });

        public ViewModelActivator Activator { get; } = new();


        /// <summary>
        /// Добавление элемента
        /// </summary>
        private void _add(T ToAdd)
        {
            try
            {
                var oc = ((IObjectContextAdapter) _repository).ObjectContext;
                var properties = Utils.ExtractColumns(oc, typeof(T))
                    .Where(property => !key.Equals(property))
                    .Select(s =>
                        new SqlProperty
                        {
                            BindingName = s.Name,
                            DisplayName = Utils.DisplayCamelCaseString(s.Name),
                            Type = s.PrimitiveType.ClrEquivalentType,
                            Nullable = s.Nullable
                        }).ToList();

                var _toAdd = _repository.Set<T>().Create();


                var prp = typeof(T).GetProperty(key.Name);

                var max = _repository.Set<T>().ToList().Max(x => (int) prp.GetValue(x));

                prp?.SetValue(_toAdd, max + 1);

                foreach (var sqlProperty in properties)
                {
                    var prop = typeof(T).GetProperty(sqlProperty.BindingName);

                    prop?.SetValue(_toAdd, prop.GetValue(ToAdd));
                }

                _repository.Set<T>().Add(_toAdd);
                _repository.SaveChanges();
                Entities.Load(_repository.Set<T>().ToList());
            }
            catch (Exception e)
            {
                var tmpException = e;
                while (tmpException.InnerException != null) tmpException = tmpException.InnerException;
                Error.OnNext($"Ошибка SQL: {tmpException.Message}");
            }
        }

        /// <summary>
        /// Удаление элемента
        /// </summary>
        /// <param name="current">Индекс элемента в таблице</param>
        /// <exception cref="ArgumentOutOfRangeException">Переданный индекс вне границ списка элементов</exception>
        private void _delete(int current)
        {
            
            if (current < 0 || current >= Entities.Count) throw new ArgumentOutOfRangeException();

            _repository.Set<T>().Remove(Entities[current]);
            _repository.SaveChanges();
            Entities.Load(_repository.Set<T>().ToList());
        }

        public DataGridColumn ViewModelColumnToViewColumn(SqlProperty c)
        {
            Binding binding;
            if (c.Reference != null)
            {
                binding = new Binding(c.Reference.Value.BindingName)
                {
                    Converter = new DisplayTemplates.DisplayTemplateValueConverter()
                };
            }
            else if (c.BindingName.ToUpper()=="ПОЛ")
            {
                binding = new Binding(c.BindingName)
                {
                    Converter = new DisplayTemplates.GenderValueConverter()
                };
            }
            else
            {
                binding = new Binding(c.BindingName);
            }
            return new DataGridTextColumn
            {
                Header = $"{(c.Reference!=null?c.Reference.Value.DisplayName:c.DisplayName)}",
                Binding = binding,
                Width = new DataGridLength(
                    1,
                    DataGridLengthUnitType.Star),
                IsReadOnly = c.Reference!=null,
                
            };
        }
    }
}