using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Reactive;
using System.Reactive.Disposables;
using DiplomModel;
using DynamicData.Annotations;
using DynamicData.Binding;
using ReactiveUI;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using Diplom;

namespace Diplom
{
    public class ClassInfo
    {
        public Класс Cls { get; set; }

        public string TransitionInfo
        {
            get
            {
                if (Cls.НомерКласса.StartsWith("11")) return "Архив";

                var result = Cls.НомерКласса;
                var resultMatch = Regex.Match(result, @"\d+");
                var classNumber = int.Parse(resultMatch.Value);
                classNumber++;
                result = Regex.Replace(result, @"\d+", classNumber.ToString());
                return result;
            }
        }
    }
}

/// <summary>
/// Модель представления переноса классов
/// </summary>
public class ClassTransitionViewModel : ReactiveObject, IActivatableViewModel
{
    private readonly DbContext _repository = new(new DiplomEntities(), true);

    public ClassTransitionViewModel()
    {
        this.WhenActivated(disposable =>
        {
            Classes.Load(_repository.Set<Класс>().Select(x => new ClassInfo {Cls = x}).ToList());
            Disposable
                .Create(() => { })
                .DisposeWith(disposable);
        });
    }


    public ObservableCollectionExtended<ClassInfo> Classes { get; set; } = new();


    public ReactiveCommand<Unit, Unit> Save => ReactiveCommand.Create(_save);

    /// <summary>
    /// Сохранение изменений
    /// </summary>
    private async void _save()
    {
        foreach (var Cls in Classes)
        {
            var result = Cls.Cls.НомерКласса;
            var resultMatch = Regex.Match(result, @"\d+");
            var classNumber = int.Parse(resultMatch.Value);
            classNumber++;
            Cls.Cls.НомерКласса = Regex.Replace(result, @"\d+", classNumber.ToString());
        }


        await _repository.SaveChangesAsync();
        Classes.Load(_repository.Set<Класс>().Select(x => new ClassInfo {Cls = x}).ToList());
    }


    public ViewModelActivator Activator { get; } = new();
}