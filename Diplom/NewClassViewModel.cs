using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using DiplomModel;
using DynamicData.Binding;
using JetBrains.Annotations;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Модель представления добавления нового класса
    /// </summary>
    public class NewClassViewModel : ReactiveObject, IActivatableViewModel
    {
        private readonly DbContext _repository = new(new DiplomEntities(), true);
        private Класс _classToAdd;

        public NewClassViewModel()
        {
            _repository.Set<Ученик>().Load();


            Select = ReactiveCommand.Create<Ученик>(teacher =>
            {
                if (UnselectedStudents.Remove(teacher)) SelectedStudents.Add(teacher);
            });
            Deselect = ReactiveCommand.Create<Ученик>(teacher =>
            {
                if (SelectedStudents.Remove(teacher)) UnselectedStudents.Add(teacher);
            });
            Add = ReactiveCommand.Create(_addClass);


            this.WhenActivated(disposables =>
            {
                Students.Load(_repository.Set<Ученик>());

                ClassToAdd = _repository.Set<Класс>().Create<Класс>();

                Teachers = _repository.Set<Сотрудник>().Where(teacher => teacher.IdДолжности == 3).ToList();

                AvailableYears.Load(_repository.Set<УчебныйГод>().ToList());


                UnselectedStudents.Load(Students.Where(x => x.Класс == null).ToList());


                /* handle activation */
                Disposable
                    .Create(() =>
                    {
                        /* handle deactivation */
                    })
                    .DisposeWith(disposables);
            });
        }

        public ObservableCollectionExtended<Ученик> UnselectedStudents { get; set; } = new();
        public ObservableCollectionExtended<Ученик> SelectedStudents { get; set; } = new();

        public ObservableCollectionExtended<Ученик> Students { get; set; } = new();

        public ObservableCollectionExtended<УчебныйГод> AvailableYears { get; set; } = new();


        public IEnumerable<Сотрудник> Teachers { get; set; }


        public ReactiveCommand<Ученик, Unit> Select { get; set; }
        public ReactiveCommand<Ученик, Unit> Deselect { get; set; }
        public ReactiveCommand<Unit, Unit> Add { get; set; }


        public Класс ClassToAdd
        {
            get => _classToAdd;
            [UsedImplicitly] set => this.RaiseAndSetIfChanged(ref _classToAdd, value);
        }

        public ViewModelActivator Activator { get; } = new();

        /// <summary>
        /// Добавление класса
        /// </summary>
        private async void _addClass()
        {
            try
            {
                foreach (var student in SelectedStudents)
                    student.НомерКласса = ClassToAdd.НомерКласса;


                await _repository.SaveChangesAsync();


                ClassToAdd = _repository.Set<Класс>().Create<Класс>();
                SelectedStudents.Clear();
            }
            catch (Exception e)
            {
                var tmpE = e;
                while (tmpE.InnerException != null) tmpE = tmpE.InnerException;
                Utils.ShowError("ОШИБКА: " + tmpE.Message);
            }
        }
    }
}