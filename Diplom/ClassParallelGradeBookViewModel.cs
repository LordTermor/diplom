using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reactive.Disposables;
using System.Text.RegularExpressions;
using DiplomModel;
using DynamicData.Binding;
using JetBrains.Annotations;
using ReactiveUI;

namespace Diplom
{
    public class ClassParallelGradeBookViewModel : ReactiveObject, IActivatableViewModel
    {
        private readonly DbContext _repository = new(new DiplomEntities(), true);
        public ObservableCollectionExtended<string> ClassParallels { get; set; } = new();
        private string _currentParallel;

        public ClassParallelGradeBookViewModel()
        {
            var arr = _repository.Set<Класс>().ToList()
                .Select(klass =>
                    Regex.Match(klass.НомерКласса, @"\d+").Value
                ).Distinct().ToArray();
            Array.Sort(arr, (s, s1) =>
                int.Parse(s) > int.Parse(s1) ? +1
                : int.Parse(s) < int.Parse(s1) ? -1
                : 0);
            ClassParallels.Load(arr);

            this.WhenActivated(disposable =>
            {
                this.WhenAnyValue(
                        x => x.CurrentParallel).Subscribe(_ => update())
                    .DisposeWith(disposable);

                Disposable
                    .Create(() => { })
                    .DisposeWith(disposable);
            });
        }


        public string CurrentParallel
        {
            get => _currentParallel;
            [UsedImplicitly] set => this.RaiseAndSetIfChanged(ref _currentParallel, value);
        }

        public ObservableCollectionExtended<Row> Rows { get; set; } = new();


        private void update()
        {
            if (CurrentParallel == null) return;
            var classes = _repository.Set<Класс>().ToList().Where(klass =>
                string.Equals(Regex.Match(klass.НомерКласса, @"\d+").Value, CurrentParallel,
                    StringComparison.Ordinal));

            var nOfStudents = classes.Sum(x => x.Ученикs.Count);

            Rows.Load(classes.Select(klass => Row.FromClass(klass, classes.ToArray())));
        }

        public struct Row
        {
            public string Class { get; set; }
            public string[] Marks { get; set; }


            public static Row FromClass(Класс klass, Класс[] klasses)
            {
                var row = new Row
                {
                    Class = klass.НомерКласса,
                    Marks = new[] {2, 3, 4, 5}.Select(i => klass.Ученикs.Sum(
                        st => st.Журналs.Count(
                            grade => grade.Оценка == i.ToString())).ToString()).ToArray()
                };
                return row;
            }
        }

        public ViewModelActivator Activator { get; } = new();
    }
}