using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Reactive;
using System.Reactive.Disposables;
using DiplomModel;
using DynamicData.Annotations;
using DynamicData.Binding;
using ReactiveUI;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Diplom
{
    /// <summary>
    /// Модель представления перевода из класса в класс
    /// </summary>
    public class TransferViewModel : ReactiveObject, IActivatableViewModel
    {
        private readonly DbContext _repository = new(new DiplomEntities(), true);
        private Класс _selectedClassLeft, _selectedClassRight;

        public TransferViewModel()
        {
            this.WhenActivated(disposable =>
            {
                Classes.Load(_repository.Set<Класс>());
                SelectedClassLeft = Classes[0];
                SelectedClassRight = Classes[0];
                this.WhenAnyValue(x => x.SelectedClassLeft,
                    x => x.SelectedClassRight).Subscribe(
                    _ => { Update(); }).DisposeWith(disposable);
                Update();
                Disposable
                    .Create(() => { })
                    .DisposeWith(disposable);
            });
        }

        public ObservableCollectionExtended<Ученик> LeftStudents { get; set; } = new();
        public ObservableCollectionExtended<Ученик> RightStudents { get; set; } = new();

        public ReactiveCommand<Ученик, Unit> ToRight =>
            ReactiveCommand.Create<Ученик>(student =>
            {
                if (LeftStudents.Remove(student)) RightStudents.Add(student);
            });

        public ReactiveCommand<Ученик, Unit> ToLeft =>
            ReactiveCommand.Create<Ученик>(student =>
            {
                if (RightStudents.Remove(student)) LeftStudents.Add(student);
            });


        public ObservableCollectionExtended<Класс> Classes { get; set; } = new();


        public ReactiveCommand<Unit, Unit> Save => ReactiveCommand.Create(_save);

        /// <summary>
        /// Сохранение изменений
        /// </summary>
        private async void _save()
        {
            foreach (var leftStudent in LeftStudents) leftStudent.НомерКласса = SelectedClassLeft.НомерКласса;

            foreach (var rightStudent in RightStudents) rightStudent.НомерКласса = SelectedClassRight.НомерКласса;
            await _repository.SaveChangesAsync();
        }


        public Класс SelectedClassLeft
        {
            get => _selectedClassLeft;
            [UsedImplicitly] set => this.RaiseAndSetIfChanged(ref _selectedClassLeft, value);
        }

        public Класс SelectedClassRight
        {
            get => _selectedClassRight;
            [UsedImplicitly] set => this.RaiseAndSetIfChanged(ref _selectedClassRight, value);
        }

        /// <summary>
        /// Обновление значений
        /// </summary>
        private void Update()
        {
            LeftStudents.Load(SelectedClassLeft.Ученикs);
            RightStudents.Load(SelectedClassRight.Ученикs);
        }

        public ViewModelActivator Activator { get; } = new();
    }
}