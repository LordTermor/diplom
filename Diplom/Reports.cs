using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using Avalonia.Controls;
using Avalonia.Layout;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DiplomModel;
using ReactiveUI;

namespace Diplom
{
    
    
    
    /// <summary>
    /// Представление страницы отчетов
    /// </summary>
    public class Reports : UserControl
    {
        private static T GridControl<T>(T obj, int row, int column) where T: Control
        {
            Grid.SetRow(obj, row);
            Grid.SetColumn(obj, column);
            return obj;
        }
        private readonly DbContext _repository = new(new DiplomEntities(), true);
        private readonly ComboBox _studentsComboBox, _subjectsComboBox,_parallelsComboBox;

        private DatePicker FromDatePicker { get; set; }
        private DatePicker ToDatePicker { get; set; }

        public Reports(Role rl)
        {
            Content = new StackPanel
            {
                Orientation = Orientation.Vertical
            };
            _subjectsComboBox = new ComboBox
            {
                Items = _repository.Set<Предмет>().AsNoTracking().ToList(),
                ItemTemplate = DisplayTemplates.GetTemplate(typeof(Предмет))
            };
            var examsReportPanel = new StackPanel
            {
                Children =
                {
                    new Button
                    {
                        Classes = new Classes("flat"),
                        Content = "Результаты экзаменов",
                        Command = ReactiveCommand.Create(CreateReport1)
                    },
                    new Label {Content = "Создать отчет о результатах экзаменов по ФИО и предмету"},
                    new StackPanel
                    {
                        Orientation   = Orientation.Horizontal,
                        Children =
                        {
                            new Label {Content = "Предмет:"},
                            _subjectsComboBox
                         
                        }
                    }
                    
                }
            };
            var gradeBookPerSubjectReportPanel = new StackPanel
            {
                Children =
                {
                    new Button
                    {
                        Classes = new Classes("flat"),
                        Content = "Оценки за предмет",
                        Command = ReactiveCommand.Create(CreateReport2)
                    },
                    new Label {Content = "Создать отчет по оценкам за предмет с количеством их получивших"}
                }
            };
            _studentsComboBox = new ComboBox
            {
                Items = _repository.Set<Ученик>().AsNoTracking().ToList(),
                ItemTemplate = DisplayTemplates.GetTemplate(typeof(Ученик))
            };
            var gradeBookReportPanel = new StackPanel
            {
                Children =
                {
                    new Button
                    {
                        Classes = new Classes("flat"),
                        Content = "Успеваемость",
                        Command = ReactiveCommand.Create(CreateReport3<GradeBookReport>)
                    },
                    new Label {Content = "Формирование отчета по успеваемости учащихся класса"},
                    new StackPanel
                    {
                     Orientation   = Orientation.Horizontal,
                     Children =
                     {
                         new Label {Content = "Учащийся:"},
                         _studentsComboBox
                         
                     }
                    }
                }
            };
            var gradeBookGroupedBySubjectReportPanel = new StackPanel
            {
                Children =
                {
                    new Button
                    {
                        Classes = new Classes("flat"),
                        Content = "Успеваемость по предметам",
                        Command = ReactiveCommand.Create(CreateReport3<GradeBookBySubject>)
                    },
                    new Label
                    {
                        Content = "Формирование отчета по успеваемости учащихся класса с группировкой по предметам"
                    }
                }
            };
            _parallelsComboBox = new ComboBox
            {
                Items = new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}
            };
            var gradeBookGroupedBySubjectAndClassReportPanel = new StackPanel
            {
                Children =
                {
                    new Button
                    {
                        Classes = new Classes("flat"),
                        Content = "Успеваемость по классам и предметам",
                        Command = ReactiveCommand.Create(CreateReport4)
                    },
                    new Label
                    {
                        Content =
                            "Формирование отчета по успеваемости в параллели с группировкой по классам и предметам с указанием оценки и количества учащихся, получивших эту оценку"
                    },
                    new StackPanel
                    {
                        Orientation   = Orientation.Horizontal,
                        Children =
                        {
                            new Label {Content = "Параллель:"},
                            _parallelsComboBox
                         
                        }
                    }
                }
            };
            List<Control> items = new();
            
            switch (rl)
            {
                case Role.FormMaster:
                    items.AddRange(new[]{
                        gradeBookGroupedBySubjectReportPanel,
                        gradeBookReportPanel
                        });
                    
                    break;
                case Role.Chief:
                    items.AddRange(
                    new []{
                            gradeBookGroupedBySubjectReportPanel,
                            examsReportPanel,
                            gradeBookReportPanel,
                            gradeBookPerSubjectReportPanel,
                            gradeBookGroupedBySubjectAndClassReportPanel
                        }
                    );
                    break;
                case Role.Teacher:
                    items.Add(gradeBookReportPanel);
                    break;
                case Role.Admin:
                    items.AddRange(
                        new []{
                            gradeBookGroupedBySubjectReportPanel,
                            examsReportPanel,
                            gradeBookReportPanel,
                            gradeBookPerSubjectReportPanel,
                            gradeBookGroupedBySubjectAndClassReportPanel
                        }
                    );
                    break;
            }

           var l1= GridControl(new Label {Content = "От:"}, 0, 0);
            var l2 = GridControl(new Label {Content = "До:"}, 1, 0);
            FromDatePicker = GridControl(new DatePicker {SelectedDate = DateTimeOffset.Now}, 0, 1);
            ToDatePicker = GridControl(new DatePicker {SelectedDate = DateTimeOffset.Now}, 1, 1);

            Grid filterPanel = new Grid
            {
                RowDefinitions = new RowDefinitions("Auto,Auto"),
                ColumnDefinitions = new ColumnDefinitions("Auto,Auto")
            };
            filterPanel.Children.AddRange(new Control[]{l1,l2,FromDatePicker,ToDatePicker});
            
            items.Add(filterPanel);
            ((StackPanel) Content).Children.AddRange(items);
            
        }

        private void CreateReport1()
        {
            DataSet2 ds = new();
            var rpt = new CrystalReport1();

            
            if (_subjectsComboBox.SelectedItem != null)
            {
                var title = (TextObject) rpt.Section1.ReportObjects["Title"];
                title.Text += " "+((Предмет) _subjectsComboBox.SelectedItem).НазваниеПредмета;
            }
            
            
            foreach (var result in _repository.Set<РезультатыОгэ>().ToList()
                .Where(x=>_subjectsComboBox.SelectedItem==null || ((Предмет) _subjectsComboBox.SelectedItem).IdПредмета == x.IdПредмета)
                .OrderBy(x=>
                $"{x.Ученик.Фамилия} {x.Ученик.Имя} {x.Ученик.Отчество}"))
            {
                var row = ds.Tables[0].Rows.Add();
                if (row is DataSet2.DataTable1Row table1Row)
                {
                    table1Row.Результаты_ЕГЭ = result.РезультатЕГЭ.ToString();
                    table1Row.Результаты_ОГЭ = result.РезультатОГЭ.ToString();
                    table1Row.Имя = result.Ученик.Имя;
                    table1Row.Фамилия = result.Ученик.Фамилия;
                    table1Row.Отчество = result.Ученик.Отчество;
                    table1Row.Предмет = result.Предмет.НазваниеПредмета;
                }
            }

            rpt.SetDataSource(ds);

            rpt.ExportToDisk(ExportFormatType.PortableDocFormat, ".\\report.pdf");

            Process.Start(@".\report.pdf");
        }

        private void CreateReport2()
        {
            ExamsPerMark set = new();

            foreach (var sbj in _repository.Set<Предмет>().ToList())
            foreach (var результатыОгэs in sbj.РезультатыОгэs.GroupBy(result => result.РезультатЕГЭ))
            {
                var row = (ExamsPerMark.ExamsPerMarkTableRow) set.ExamsPerMarkTable.Rows.Add();

                row.Предмет = sbj.НазваниеПредмета;
                row.Оценка = результатыОгэs.Key.ToString();
                row.Количество_получивших = результатыОгэs.Count().ToString();
            }

            ExamsPerMarkReport rp = new();
            rp.SetDataSource(set);
            rp.ExportToDisk(ExportFormatType.PortableDocFormat, ".\\report1.pdf");
            Process.Start(@".\report1.pdf");
        }

        private void CreateReport3<T>() where T : ReportClass, new()
        {
            GradeBookDataSet set = new();
            T rpt = new();
            

            if (_studentsComboBox.SelectedItem != null)
            {
                TextObject title = (TextObject)rpt.ReportDefinition.Sections["Section1"].ReportObjects["Title"];

                var student = (Ученик) _studentsComboBox.SelectedItem;
                title.Text = $"Сведения по успеваемости ученика {student.Класс.НомерКласса} класса " +
                             $"{student.Фамилия} {student.Имя.First()}.{student.Отчество.First()}. " +
                             $"за период {FromDatePicker.SelectedDate?.Date.ToShortDateString()}-{ToDatePicker.SelectedDate?.Date.ToShortDateString()}";
                
                FieldObject fioText = (FieldObject)rpt.ReportDefinition.Sections["Section3"].ReportObjects["ФИО1"];
                FieldHeadingObject fioHeader = (FieldHeadingObject)rpt.ReportDefinition.Sections["Section2"].ReportObjects["Text2"];
                fioText.Color = fioHeader.Color = Color.Transparent;
            }
            
            
            
            foreach (var sbj in _repository.Set<Журнал>()
                .ToList()
                .Where(x=>_studentsComboBox.SelectedItem==null 
                          || x.Ученик.IdУченика== ((Ученик) _studentsComboBox.SelectedItem).IdУченика)
                .Where(x=>x.Дата.InclusiveBetween(FromDatePicker.SelectedDate?.Date,ToDatePicker.SelectedDate?.Date))
                .OrderBy(x=>x.Дата))
            {
                var row = (GradeBookDataSet.DataTable1Row) set.DataTable1.Rows.Add();

                row.Дата = sbj.Дата.ToShortDateString();
                row.ФИО = $"{sbj.Ученик.Фамилия} {sbj.Ученик.Имя} {sbj.Ученик.Отчество}";
                row.Предмет = sbj.Предмет.НазваниеПредмета;
                row.Оценка = sbj.Оценка;
            }

            rpt.SetDataSource(set);
            rpt.ExportToDisk(ExportFormatType.PortableDocFormat, @".\report2.pdf");
            Process.Start(@".\report2.pdf");
        }

        private void CreateReport4()
        {
            ClassParallelGradeBookDataSet set = new();
            ClassParallelGradeBookReport rpt = new();

            var to = (TextObject) rpt.Section1.ReportObjects["Title"];
            if (_parallelsComboBox.SelectedItem == null)
            {
                to.Text = "Сведения по успеваемости в параллели в период " +
                          $"{FromDatePicker.SelectedDate?.Date.ToShortDateString()}-{ToDatePicker.SelectedDate?.Date.ToShortDateString()}";

            }
            else
            {
                to.Text = $"Сведения по успеваемости в параллели {_parallelsComboBox.SelectedItem} в период " +
                          $"{FromDatePicker.SelectedDate?.Date.ToShortDateString()}-{ToDatePicker.SelectedDate?.Date.ToShortDateString()}";
            }

            foreach (var klass in _repository.Set<Класс>().ToList())
            foreach (var subject in _repository.Set<Предмет>().ToList())
            {
                if (!klass.Ученикs.Any(st => st.Журналs.Any(gr => gr.Предмет == subject))) continue;
                foreach (var mark in new[] {2, 3, 4, 5})
                {
                    var parallel = Regex.Match(klass.НомерКласса, @"\d+").Value;
                    
                    if(parallel != _parallelsComboBox.SelectedItem?.ToString()) continue; 
                    
                    var row = (ClassParallelGradeBookDataSet.DataTable1Row) set.DataTable1.Rows.Add();

                    row.Параллель = parallel;
                    row.Класс = klass.НомерКласса;
                    row.Предмет = subject.НазваниеПредмета;
                    row.Оценка = mark.ToString();
                    row.Количество = klass.Ученикs.Sum(
                        st => st.Журналs
                            .Where(x=>x.Дата.InclusiveBetween(FromDatePicker.SelectedDate?.Date,ToDatePicker.SelectedDate?.Date)).Count(
                            grade => grade.Предмет == subject &&
                                     grade.Оценка == mark.ToString())).ToString();
                    Debug.WriteLine(
                        $"{row.Параллель} {row.Класс} {row.Предмет} {row.Оценка} {row.Количество}");
                }
            }

            rpt.SetDataSource(set);
            rpt.ExportToDisk(ExportFormatType.PortableDocFormat, @".\report3.pdf");
            Process.Start(@".\report3.pdf");
        }
    }
}