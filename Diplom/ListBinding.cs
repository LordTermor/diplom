#nullable enable
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Avalonia.Controls;
using Avalonia.Data;
using ReactiveUI;

namespace Diplom
{
    public class ListBinding : IBindingTypeConverter
    {
        public int GetAffinityForObjects(Type fromType, Type toType)
        {
            if (fromType == typeof(IEnumerable)) return 100;

            return 0;
        }

        public bool TryConvert(object? from, Type toType, object? conversionHint, out object? result)
        {
            result = from;
            return true;
        }
    }

    public class ColumnBindingConverter : IBindingTypeConverter
    {
        public int GetAffinityForObjects(Type fromType, Type toType)
        {
            if (typeof(IEnumerable<SqlProperty>).IsAssignableFrom(fromType) &&
                typeof(IEnumerable).IsAssignableFrom(toType)) return 100;

            return 0;
        }

        public bool TryConvert(object? from, Type toType, object? conversionHint, out object? result)
        {
            var c = (IEnumerable<SqlProperty>) from!;

            result = c.Select(el => new DataGridTextColumn
            {
                Header = $"{el.DisplayName}",
                Binding = new Binding(el.BindingName ?? ""),
                Width = new DataGridLength(
                    1,
                    DataGridLengthUnitType.Star)
            }).ToList();
            return true;
        }
    }
}