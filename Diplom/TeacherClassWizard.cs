using System.Reactive.Disposables;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Представление добавления учителя к классу
    /// </summary>
    public class TeacherClassWizard : ReactiveUserControl<TeacherClassWizardViewModel>
    {
        private ListBox AllTeachersListBox => this.FindControl<ListBox>("AllTeachersListBox");
        private ComboBox ClassesComboBox => this.FindControl<ComboBox>("ClassesComboBox");

        private Button LeftButton => this.FindControl<Button>("LeftButton");
        private Button RightButton => this.FindControl<Button>("RightButton");
        private Button SaveButton => this.FindControl<Button>("SaveButton");
        private ListBox SelectedTeachersListBox => this.FindControl<ListBox>("SelectedTeachersListBox");

        public TeacherClassWizard()
        {
            Tag = "Учителя класса";
            ViewModel = new TeacherClassWizardViewModel();


            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel,
                        model => model.UnselectedTeachers,
                        view => view.AllTeachersListBox.Items)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel,
                        model => model.SelectedTeachers,
                        wizard => wizard.SelectedTeachersListBox.Items)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel,
                        model => model.Classes,
                        wizard => wizard.ClassesComboBox.Items)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.Select,
                        wizard => wizard.RightButton,
                        AllTeachersListBox.GetObservable(SelectingItemsControl.SelectedItemProperty))
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.Deselect,
                        wizard => wizard.LeftButton,
                        SelectedTeachersListBox.GetObservable(SelectingItemsControl.SelectedItemProperty))
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.Save,
                        wizard => wizard.SaveButton)
                    .DisposeWith(disposable);


                this.Bind(ViewModel,
                        model => model.SelectedClass,
                        wizard => wizard.ClassesComboBox.SelectedItem)
                    .DisposeWith(disposable);
            });

            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}