﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using Avalonia.Controls;
using Avalonia.Layout;
using Avalonia.Markup.Xaml;
using DiplomModel;
using DynamicData.Binding;
using JetBrains.Annotations;
using ReactiveUI;

namespace Diplom
{
    public sealed class MainWindow : Window
    {
        private IEnumerable<IControl> GetButtons(Role rl)
        {
            var controls = rl switch
            {
                Role.Chief => new UserControl[]
                {
                    new GradeBook(true, GradeBook.Filters.No, "Успеваемость класса по предмету"),
                    new GradeBook(true, GradeBook.Filters.Student, "Успеваемость ученика"),
                    new GradeBook(true, GradeBook.Filters.OnlyFinalClasses, "Итоговая успеваемость выпускных классов"),
                    new ClassParallelGradeBook("Успеваемость в параллели"),
                    new Exams(true, "Результат экзаменов по указанным предметам")
                },
                Role.Teacher => new UserControl[]
                {
                    new GradeBook(true, GradeBook.Filters.No, "Успеваемость класса по предмету")
                },
                Role.FormMaster => new UserControl[]
                {
                    new GradeBook(true, GradeBook.Filters.No, "Успеваемость класса по предмету"),
                    new GradeBook(true, GradeBook.Filters.Student, "Успеваемость ученика"),
                    new GradeBook(true, GradeBook.Filters.OnlyFinalClasses, "Итоговая успеваемость выпускных классов"),
                    new Exams(true, "Результат экзаменов по указанным предметам"),
                },
                Role.Admin => new UserControl[]
                {
                    new GradeBook(true, GradeBook.Filters.No, "Успеваемость класса по предмету"),
                    new GradeBook(true, GradeBook.Filters.Student, "Успеваемость ученика"),
                    new GradeBook(true, GradeBook.Filters.OnlyFinalClasses, "Итоговая успеваемость выпускных классов"),
                    new ClassParallelGradeBook("Успеваемость в параллели"),
                    new Exams(true, "Результат экзаменов по указанным предметам")
                },
                _ => new UserControl[]{}
                
            };

            return controls.Select(control =>
            {
                var backPseudoTab = new TabItem()
                {
                    Header = "Назад"
                };
                backPseudoTab.PropertyChanged += (_, args) =>
                {
                    if (args.NewValue == null || args.Property != TabItem.IsSelectedProperty ||
                        !(bool) args.NewValue) return;

                    var tab = new TabItem
                    {
                        Content = new StackPanel
                        {
                            Orientation = Orientation.Vertical,
                            VerticalAlignment = VerticalAlignment.Center
                        }
                    };
                    ((StackPanel) tab.Content).Children.AddRange(GetButtons(rl));
                    Items.Queries.Add(tab);
                    Items.Queries.RemoveRange(0, 2);
                };

                var button = new Button
                {
                    Classes = new Classes("flat"),
                    Width = 450,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Content = control.Tag?.ToString(),

                    Command = ReactiveCommand.Create(() =>
                    {
                        Items.Queries.Load(new[]
                        {
                            new TabItem {Header = control.Tag?.ToString(), Content = control, IsSelected = true}
                        });
                        Items.Queries.Insert(0, backPseudoTab);
                    })
                };
                return button;
            });
        }

        private readonly Dictionary<Role, IEnumerable<UserControl>> _availableDicts = new()
        {
            {
                Role.Secretary, new UserControl[]
                {
                    new SqlTable<Должность>(),
                    new SqlTable<Ученик>(),
                    new SqlTable<Предмет>(),
                    new SqlTable<Сотрудник>()
                }
            },
            {
                Role.Chief, new UserControl[]
                {
                }
            },
            {
                Role.Teacher, new UserControl[]
                {
                }
            },
            {
                Role.FormMaster, new UserControl[]
                {
                }
            },
            {
                Role.Admin, new UserControl[]
                {
                    new SqlTable<Ученик>(),
                    new SqlTable<Должность>(),
                    new SqlTable<Предмет>(),
                    new SqlTable<Сотрудник>()
                }
            }
        };

        private readonly Dictionary<Role, IEnumerable<UserControl>> _availableOpers = new()
        {
            {
                Role.Secretary, new UserControl[]
                {
                    new NewClassWizard(),
                    new Transfer()
                }
            },
            {
                Role.Chief, new UserControl[]
                {
                    new TeacherClassWizard(),
                    new NewClassWizard()
                }
            },
            {
                Role.Teacher, new UserControl[]
                {
                    new GradeBook(),
                }
            },
            {
                Role.FormMaster, new UserControl[]
                {
                    new GradeBook(),
                    new Exams()
                }
            },
            {
                Role.Admin, new UserControl[]
                {
                    new NewClassWizard(),
                    new Transfer(),
                    new GradeBook(),
                    new Exams(),
                    new ClassTransition()
                }
            }
        };

        private readonly Dictionary<Role, IEnumerable<UserControl>> _availableRepors = new()
        {
            {
                Role.Secretary, new UserControl[]
                {
                }
            },
            {
                Role.Chief, new UserControl[]
                {
                    new Reports(Role.Chief)
                }
            },
            {
                Role.Teacher, new UserControl[]
                {
                    new Reports(Role.Teacher)
                }
            },
            {
                Role.FormMaster, new UserControl[]
                {
                    new Reports(Role.FormMaster)
                }
            },
            {
                Role.Admin, new UserControl[]
                {
                    new Reports(Role.Admin)
                }
            }
        };

        private readonly Dictionary<Role, IEnumerable<UserControl>> _availableQueries = new()
        {
            {
                Role.Secretary, new UserControl[]
                {
                }
            },
            {
                Role.Chief, new[]
                {
                    new UserControl
                    {
                        Content = new StackPanel
                        {
                            Orientation = Orientation.Vertical,
                            VerticalAlignment = VerticalAlignment.Center
                        }
                    }
                }
            },
            {
                Role.Teacher, new[]
                {
                    new UserControl
                    {
                        Content = new StackPanel
                        {
                            Orientation = Orientation.Vertical,
                            VerticalAlignment = VerticalAlignment.Center
                        }
                    }
                }
            },
            {
                Role.FormMaster, new[]
                {
                    new UserControl
                    {
                        Content = new StackPanel
                        {
                            Orientation = Orientation.Vertical,
                            VerticalAlignment = VerticalAlignment.Center
                        }
                    }
                }
            },
            {
                Role.Admin, new[]
                {
                    new UserControl
                    {
                        Content = new StackPanel
                        {
                            Orientation = Orientation.Vertical,
                            VerticalAlignment = VerticalAlignment.Center
                        }
                    }
                }
            }
        };

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            ((StackPanel) _availableQueries[Role.Chief].First().Content).Children.AddRange(GetButtons(Role.Chief));
            ((StackPanel) _availableQueries[Role.FormMaster].First().Content).Children.AddRange(
                GetButtons(Role.FormMaster));
            ((StackPanel) _availableQueries[Role.Teacher].First().Content).Children.AddRange(
                GetButtons(Role.Teacher));
            ((StackPanel) _availableQueries[Role.Admin].First().Content).Children.AddRange(GetButtons(Role.Chief));


            App.CurrentRole.Subscribe(InitializeTabList);
            LoggedIn.OnNext(false);

            App.CurrentRole.OnNext(Role.Admin);
        }

        private class TabCategories
        {
            public ObservableCollectionExtended<TabItem> Queries { get; set; } = new();
            public ObservableCollectionExtended<TabItem> Opers { get; set; } = new();
            public ObservableCollectionExtended<TabItem> Dicts { get; set; } = new();
            public ObservableCollectionExtended<TabItem> Reports { get; set; } = new();
        }


        private TabCategories Items { get; set; } = new();
        private Subject<bool> LoggedIn { get; } = new();


        private void InitializeTabList(Role role)
        {
            if (role == Role.LoggedOut)
            {
                LoggedIn.OnNext(false);
                Items.Dicts.Load(new TabItem[]
                {
                    new()
                    {
                        Header = "Домашняя страница",
                        Content = new AuthControl()
                    }
                });

                this.FindControl<TabControl>("SideBar").SelectedIndex = 0;
                return;
            }

            LoggedIn.OnNext(true);
            Items.Dicts.Load(
                _availableDicts[role].Select(control =>
                    new TabItem
                    {
                        Header = control.Tag?.ToString(),
                        Content = control
                    })
            );
            Items.Opers.Load(
                _availableOpers[role].Select(control =>
                    new TabItem
                    {
                        Header = control.Tag?.ToString(),
                        Content = control
                    })
            );
            Items.Reports.Load(
                _availableRepors[role].Select(control =>
                    new TabItem
                    {
                        Header = control.Tag?.ToString(),
                        Content = control
                    })
            );
            Items.Queries.Load(
                _availableQueries[role].Select(control =>
                    new TabItem
                    {
                        Header = control.Tag?.ToString(),
                        Content = control
                    }));
        }


        [UsedImplicitly]
        private void OnLogoutClicked()
        {
            App.CurrentRole.OnNext(Role.LoggedOut);
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);

            Items.Dicts.Load(new ObservableCollectionExtended<TabItem>
            {
                new()
                {
                    Header = "Домашняя страница",
                    Content = new AuthControl()
                }
            });
        }
    }
}