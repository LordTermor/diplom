﻿using System.Collections.Generic;
using System.Reactive.Subjects;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;
using Splat;

namespace Diplom
{
    public class App : Application
    {
        public static readonly Subject<Role> CurrentRole = new();

        public AuthViewModel AuthViewModel
        {
            get => default;
            set { }
        }

        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
                desktop.MainWindow = new MainWindow();
        }

        public static int Main(string[] args)
        {
            return BuildAvaloniaApp().StartWithClassicDesktopLifetime(args);
        }

        private static AppBuilder BuildAvaloniaApp()
        {
            Locator.CurrentMutable.RegisterConstant(new ListBinding(), typeof(IBindingTypeConverter));
            Locator.CurrentMutable.RegisterConstant(new ColumnBindingConverter(), typeof(IBindingTypeConverter));

            return AppBuilder.Configure<App>()
                .UseReactiveUI()
                .UsePlatformDetect()
                .LogToTrace();
        }
    }
}