﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;

namespace Diplom
{
    public static class Utils
    {
        public static bool InclusiveBetween(this IComparable a, IComparable b, IComparable c)
        {
            return a.CompareTo(b) >= 0 && a.CompareTo(c) <= 0;
        }

        public static string SHA256Hash(string str)
        {
            return GetHash(SHA256.Create(), str);
        }

        private static string GetHash(HashAlgorithm hashAlgorithm, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            var data = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            var sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            foreach (var t in data) sBuilder.Append(t.ToString("x2"));

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static List<EdmProperty> ExtractColumns(ObjectContext context, Type type)
        {
            var metadata = context.MetadataWorkspace;

            // Get the part of the model that contains info about the actual CLR types
            var objectItemCollection = (ObjectItemCollection) metadata.GetItemCollection(DataSpace.OSpace);

            // Get the entity type from the model that maps to the CLR type
            var entityTypeList = metadata
                .GetItems<EntityType>(DataSpace.OSpace);
            var entityType = entityTypeList.Single(e => objectItemCollection.GetClrType(e) == type);

            // Get the entity set that uses this entity type
            var entitySet = metadata
                .GetItems<EntityContainer>(DataSpace.CSpace)
                .Single()
                .EntitySets
                .Single(s => s.ElementType.Name == entityType.Name);

            // Find the mapping between conceptual and storage model for this entity set
            var mapping = metadata.GetItems<EntityContainerMapping>(DataSpace.CSSpace)
                .Single()
                .EntitySetMappings
                .Single(s => s.EntitySet == entitySet);

            // Find all properties (column) that are mapped
            var columnName = mapping
                .EntityTypeMappings.Single()
                .Fragments.Single()
                .PropertyMappings
                .OfType<ScalarPropertyMapping>()
                .Select(t => t.Property).ToList();

            return columnName;
        }

        public static void ShowError(string errorMsg)
        {
            if (errorMsg == null) return;
            var win = new Window
            {
                Content = new TextBox {Text = errorMsg},
                CanResize = false,
                SizeToContent = SizeToContent.WidthAndHeight,
                Padding = new Thickness(25),
                Title = "Ошибка"
            };
            win.ShowDialog(((IClassicDesktopStyleApplicationLifetime) Application.Current.ApplicationLifetime)
                .MainWindow);
        }

        public static IEnumerable<NavigationProperty> GetNavigationProperties<TEntity>(ObjectContext objectContext)
            where TEntity : class
        {
            var set = objectContext.CreateObjectSet<TEntity>();
            return set.EntitySet.ElementType.NavigationProperties.ToList();
        }

        public static string DisplayCamelCaseString(string camelCase)
        {
            var chars = new List<char> {camelCase[0]};
            foreach (var c in camelCase.Skip(1))
            {
                if (char.IsUpper(c)) chars.Add(' ');
                chars.Add(c);
            }

            return new string(chars.ToArray());
        }
    }
}