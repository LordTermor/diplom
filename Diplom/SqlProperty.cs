using System;
using System.Data.Entity.Core.Metadata.Edm;

namespace Diplom
{
    public struct ReferencedProperty
    {
        public bool Nullable { get; set; }
        public Type Type { get; set; }
        public string BindingName { get; set; }
        public string DisplayName { get; set; }

    }
    public struct SqlProperty
    {
        public bool Nullable { get; set; }
        public Type Type { get; set; }
        public string DisplayName { get; set; }
        public string BindingName { get; set; }
        
        public ReferencedProperty? Reference { get; set; }

        public static SqlProperty FromEdmProperty(EdmProperty s)
        {
            return new()
            {
                BindingName = s.Name,
                DisplayName = Utils.DisplayCamelCaseString(s.Name),
                Type = s.PrimitiveType.ClrEquivalentType,
                Nullable = s.Nullable
            };
        }
    }
}