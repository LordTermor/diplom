using System;
using System.Linq;
using System.Reactive.Disposables;
using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using DynamicData;
using DynamicData.Binding;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Представление успеваемости
    /// </summary>
    public class GradeBook : ReactiveUserControl<GradeBookViewModel>
    {
        [Flags]
        public enum Filters
        {
            No,
            Student,
            OnlyFinalClasses
        }

        public GradeBook() : this(false)
        {
        }

        public GradeBook(bool isQuery, Filters flt = Filters.No, string name = "Журнал успеваемости")
        {
            Tag = name;

            ViewModel = new GradeBookViewModel();


            this.WhenActivated(disposable =>
            {
                ViewModel.Classes
                    .ToObservableChangeSet()
                    .Filter(x => flt != Filters.OnlyFinalClasses || x.НомерКласса.StartsWith("11"))
                    .ToCollection()
                    .BindTo(this, view => view.ClassComboBox.Items).DisposeWith(disposable);

                this.OneWayBind(ViewModel,
                        model => model.Subjects,
                        book => book.SubjectComboBox.Items)
                    .DisposeWith(disposable);
                this.OneWayBind(ViewModel,
                        model => model.Rows,
                        book => book.GradebookDataGrid.Items)
                    .DisposeWith(disposable);

                this.Bind(ViewModel,
                        model => model.DateFilterFrom,
                        book => book.FromDatePicker.SelectedDate)
                    .DisposeWith(disposable);
                this.Bind(ViewModel,
                        model => model.DateFilterTo,
                        book => book.ToDatePicker.SelectedDate)
                    .DisposeWith(disposable);

                this.Bind(ViewModel,
                        model => model.CurrentClass,
                        book => book.ClassComboBox.SelectedItem)
                    .DisposeWith(disposable);

                this.Bind(ViewModel,
                        model => model.CurrentSubject,
                        book => book.SubjectComboBox.SelectedItem)
                    .DisposeWith(disposable);

                this.Bind(ViewModel,
                        model => model.FilterEnabled,
                        view => view.FilterEnabledCheckBox.IsChecked)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel,
                        model => model.AvailableStudents,
                        view => view.StudentComboBox.Items)
                    .DisposeWith(disposable);

                this.Bind(ViewModel,
                        model => model.Student,
                        view => view.StudentComboBox.SelectedItem)
                    .DisposeWith(disposable);

                ViewModel.Columns
                    .ToObservableChangeSet()
                    .ToCollection()
                    .Subscribe(column =>
                    {
                        GradebookDataGrid.Columns.Clear();
                        GradebookDataGrid.Columns.AddRange(column.Select(el => new DataGridTextColumn
                        {
                            Header = $"{el.DisplayName}",
                            Binding = new Binding(el.BindingName ?? "", BindingMode.TwoWay),
                            Width = new DataGridLength(
                                1,
                                DataGridLengthUnitType.Star),
                            IsReadOnly = false
                        }).ToList());
                    }).DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.SaveCommand,
                        view => view.SaveButton)
                    .DisposeWith(disposable);
            });

            InitializeComponent();

            if (isQuery)
            {
                if (!flt.HasFlag(Filters.Student)) StudentComboBox.IsVisible = StudentLabel.IsVisible = false;
                SaveButton.IsVisible = false;
                ViewModel.FilterEnabled = true;
            }
            else
            {
                StudentComboBox.IsVisible = StudentLabel.IsVisible = FilterPanel.IsVisible = false;
            }
        }

        private ComboBox ClassComboBox => this.FindControl<ComboBox>("ClassComboBox");
        private ComboBox SubjectComboBox => this.FindControl<ComboBox>("SubjectComboBox");
        private DataGrid GradebookDataGrid => this.FindControl<DataGrid>("GradebookDataGrid");
        private DatePicker FromDatePicker => this.FindControl<DatePicker>("FromDatePicker");
        private DatePicker ToDatePicker => this.FindControl<DatePicker>("ToDatePicker");
        private Button SaveButton => this.FindControl<Button>("SaveButton");
        private CheckBox FilterEnabledCheckBox => this.FindControl<CheckBox>("FilterEnabledCheckBox");
        private StackPanel FilterPanel => this.FindControl<StackPanel>("FilterPanel");
        private ComboBox StudentComboBox => this.FindControl<ComboBox>("StudentComboBox");
        private Label StudentLabel => this.FindControl<Label>("StudentLabel");

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}