﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Reactive.Disposables;
using DiplomModel;
using ReactiveUI;
using Ubiety.Dns.Core;

namespace Diplom
{
    
    public class MainWindowViewModel : ReactiveObject, IActivatableViewModel
    {
        private static string DisplayCamelCaseString(string camelCase)
        {
            List<char> chars = new List<char> {camelCase[0]};
            foreach(char c in camelCase.Skip(1))
            {
                if (char.IsUpper(c))
                {
                    chars.Add(' ');
                }
                chars.Add(c);
            }
		
            return new string(chars.ToArray());
        }

       
        public ViewModelActivator Activator { get; } = new ViewModelActivator();

        public MainWindowViewModel()
        {
            d = ent.Должностьs.ToList();

            Columns = Utils.ExtractColumns<Должность>(new DiplomEntities())
                .Select(s => new Column{Property = s, Header = DisplayCamelCaseString(s)}).ToList();
            

            
            this.WhenActivated(disposables =>
            {
                /* Handle activation */
                Disposable
                    .Create(() => { /* Handle deactivation */ })
                    .DisposeWith(disposables);
            });
        }

        public class Column
        {
            public string Header { get; set; }
            public string Property { get; set; }
        }
        public List<Column> Columns { get; set; }
        public List<Должность> d { get; set; }
        private DiplomEntities ent = new DiplomEntities();
    }
}