using System.Reactive.Disposables;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Controls.Templates;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Модель представления переноса классов
    /// </summary>
    public class ClassTransition : ReactiveUserControl<ClassTransitionViewModel>
    {
        public ClassTransition()
        {
            Tag = "Перенос";
            ViewModel = new ClassTransitionViewModel();
            InitializeComponent();


            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel,
                        vm => vm.Classes,
                        view => view.ClassesListBox.Items)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.Save,
                        view => view.SaveButton)
                    .DisposeWith(disposable);
            });
        }

        private ListBox ClassesListBox => this.FindControl<ListBox>("ClassesListBox");

        private Button SaveButton => this.FindControl<Button>("SaveButton");


        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}