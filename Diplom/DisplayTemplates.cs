using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;
using System.Windows;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Templates;
using Avalonia.Data.Converters;
using DiplomModel;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Набор шаблонов для строкового представления сущностных классов
    /// </summary>
    public static class DisplayTemplates
    {
        private static Dictionary<Type, Func<object, IObservable<string>>> _templates = new()
        {
            {
                typeof(Предмет),
                x => ((Предмет) x).WhenAnyValue(x1 => x1.НазваниеПредмета)
            },
            {
                typeof(Ученик),
                x => ((Ученик) x).WhenAnyValue(
                    x1 => x1.Фамилия,
                    x1 => x1.Имя,
                    x1 => x1.Отчество,
                    (f, i, o) => $"{f} {i} {o}")
            },
            {
                typeof(Сотрудник),
                x => ((Сотрудник) x).WhenAnyValue(
                    x1 => x1.Фамилия,
                    x1 => x1.Имя,
                    x1 => x1.Отчество,
                    (f, i, o) => $"{f} {i} {o}")
            },
            {
                typeof(ВидРаботы),
                x => ((ВидРаботы) x).WhenAnyValue(x1 => x1.НазваниеВидаРаботы)
            },
            {
                typeof(УчебныйГод),
                x => ((УчебныйГод) x).WhenAnyValue(x1 => x1.УчебныйГод1)
            },
            {
                typeof(Родитель),
                x => ((Родитель) x).WhenAnyValue(
                    x1 => x1.Фамилия,
                    x1 => x1.Имя,
                    x1 => x1.Отчество,
                    (f, i, o) => $"{f} {i} {o}")
            },
            {
                typeof(Образование),
                x => ((Образование) x).WhenAnyValue(x1 => x1.НазваниеСпециальности)
            },
            {
                typeof(Кабинет),
                x => ((Кабинет) x).WhenAnyValue(x1 => x1.НомерКабинета)
            },
            {
                typeof(Класс),
                x => ((Класс) x).WhenAnyValue(x1 => x1.НомерКласса)
            },
            {
                typeof(СтепеньРодства),
                x => ((СтепеньРодства) x).WhenAnyValue(x1 => x1.НазваниеСтепениРодства)
            },
            {
                typeof(Организация),
                x => ((Организация) x).WhenAnyValue(x1 => x1.НазваниеОрганизации)
            },
            {
                typeof(Вуз),
                x => ((Вуз) x).WhenAnyValue(x1 => x1.НазваниеВУЗа)
            },
            {
                typeof(Должность),
                x => ((Должность) x).WhenAnyValue(x1 => x1.НазваниеДолжности)
            }
        };

        public static FuncDataTemplate GetTemplate(Type t)
        {
            return _templates.ContainsKey(t)
                ? new FuncDataTemplate(x => x.GetType() == t, (x, _) => new TextBlock
                {
                    [!TextBlock.TextProperty] = _templates[t](x).ToBinding()
                })
                : null;
        }
        public class DisplayTemplateValueConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                if (targetType != typeof(string)) throw new ArgumentException();
                if (value is string) return value;
                if (!_templates.ContainsKey(value.GetType())) return value.ToString();
                
                var template = _templates[value.GetType()];
                
                var task = template(value).Take(1).ToTask();
                Task.WaitAll(task);
                return task.Result;
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return DependencyProperty.UnsetValue;
            }
        }
        public class GenderValueConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                if (targetType != typeof(string) || value is not bool gender) throw new ArgumentException();
                return gender ? "Жен" : "Муж";

            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                if (targetType != typeof(bool)) throw new ArgumentException();
                return value.ToString().ToUpper() switch
                {
                    "М" => false,
                    "МУЖ" => false,
                    "Ж" => true,
                    "ЖЕН" => true,
                    _ => DependencyProperty.UnsetValue
                };
            }
        }
    }
}