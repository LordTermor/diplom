using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using DiplomModel;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Представление экзаменов
    /// </summary>
    public class Exams : ReactiveUserControl<ExamsViewModel>
    {
        public Exams() : this(false)
        {
        }

        public Exams(bool isQuery, string name = "Экзамены")
        {
            Tag = name;
            ViewModel = new ExamsViewModel();
            InitializeComponent();
            if (isQuery)
            {
                SaveButton.IsVisible = false;
                AddButton.IsVisible = false;
                RemoveButton.IsVisible = false;
            }
            else
            {
                SubjectComboBox.IsVisible = false;
            }


            this.WhenActivated(disposable =>
            {
                AddButton.Command = ReactiveCommand.Create(_add);

                this.OneWayBind(ViewModel,
                        model => model.Classes,
                        view => view.ClassComboBox.Items)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel,
                        model => model.Students,
                        view => view.StudentComboBox.Items)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel,
                        model => model.Subjects,
                        view => view.SubjectComboBox.Items)
                    .DisposeWith(disposable);

                this.Bind(ViewModel,
                        model => model.CurrentClass,
                        view => view.ClassComboBox.SelectedItem)
                    .DisposeWith(disposable);

                this.Bind(ViewModel,
                        model => model.CurrentStudent,
                        view => view.StudentComboBox.SelectedItem)
                    .DisposeWith(disposable);

                this.Bind(ViewModel,
                        model => model.CurrentSubject,
                        view => view.SubjectComboBox.SelectedItem)
                    .DisposeWith(disposable);

                this.Bind(ViewModel,
                        model => model.Results,
                        view => view.ExamsDataGrid.Items)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.Save,
                        view => view.SaveButton)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel,
                        model => model.Remove,
                        view => view.RemoveButton)
                    .DisposeWith(disposable);
            });
        }

        private ComboBox ClassComboBox => this.FindControl<ComboBox>("ClassComboBox");
        private ComboBox StudentComboBox => this.FindControl<ComboBox>("StudentComboBox");
        private ComboBox SubjectComboBox => this.FindControl<ComboBox>("SubjectComboBox");
        private DataGrid ExamsDataGrid => this.FindControl<DataGrid>("ExamsDataGrid");
        private Button SaveButton => this.FindControl<Button>("SaveButton");
        private Button RemoveButton => this.FindControl<Button>("RemoveButton");
        private Button AddButton => this.FindControl<Button>("AddButton");

        private async void _add()
        {
            var oc = new DiplomEntities();

            var properties = Utils.ExtractColumns(oc, typeof(РезультатыОгэ))
                .Where(property => property.Name != "IdРезультата")
                .Select(s =>
                    new SqlProperty
                    {
                        BindingName = s.Name,
                        DisplayName = Utils.DisplayCamelCaseString(s.Name),
                        Type = s.PrimitiveType.ClrEquivalentType,
                        Nullable = s.Nullable
                    }).ToList();

            properties.SortOverrides();
            var fks = Utils.GetNavigationProperties<РезультатыОгэ>(oc);

            properties = properties.Select(x =>
            {
                foreach (var fk in fks)
                {
                    if (!fk.GetDependentProperties().Any() ||
                        x.BindingName != fk.GetDependentProperties().First().Name) continue;
                    x.Reference = new ReferencedProperty()
                    {
                        BindingName = fk.TypeUsage.EdmType.Name,
                        DisplayName = Utils.DisplayCamelCaseString(fk.TypeUsage.EdmType.Name),
                        Type = Type.GetType(fk.TypeUsage.EdmType.FullName),
                    };
                    break;
                }

                return x;
            }).ToList();

            var dialog = new SqlAddDialog<РезультатыОгэ>(properties);
            var journal = await dialog.ShowDialog<РезультатыОгэ>(
                ((IClassicDesktopStyleApplicationLifetime) Application.Current.ApplicationLifetime).MainWindow);
            if (journal != null) await ViewModel.Add.Execute(journal);
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}