using System.Collections.Generic;

namespace Diplom
{
    public static class PropertyOverrides
    {
        private class PropertyOverride
        {
            public int? Position { get; set; }
            public string[] NextPositions { get; set; }
        }

        private static readonly Dictionary<string, PropertyOverride> Overrides = new()
        {
            {"Фамилия", new PropertyOverride() {Position = 0, NextPositions = new[] {"Имя", "Отчество"}}}
        };

        public static void SortOverrides(this IList<SqlProperty> properties)
        {
            for (var index = 0; index < properties.Count; index++)
            {
                var property = properties[index];
                if (Overrides.ContainsKey(property.BindingName) && Overrides[property.BindingName].Position != null)
                {
                    properties.RemoveAt(index);
                    properties.Insert(Overrides[property.BindingName].Position.Value, property);
                }
            }

            for (var index = 0; index < properties.Count; index++)
            {
                var property = properties[index];
                if (Overrides.ContainsKey(property.BindingName))
                {
                    var left = Overrides[property.BindingName].NextPositions.Length;
                    for (var i = 0; i < Overrides[property.BindingName].NextPositions.Length; i++)
                    {
                        var binding = Overrides[property.BindingName].NextPositions[i];
                        var propsSize = properties.Count - 1;

                        while (properties[propsSize].BindingName != binding && propsSize >= 0) propsSize--;

                        if (propsSize < 0) continue;

                        var prop = properties[propsSize];
                        properties.RemoveAt(propsSize);
                        properties.Insert(index + Overrides[property.BindingName].NextPositions.Length - left,
                            prop);
                    }
                }
            }
        }
    }
}