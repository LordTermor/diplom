using System.Collections.Generic;
using System.Reactive;
using DynamicData.Kernel;
using ReactiveUI;

namespace Diplom
{
    /// <summary>
    /// Модель представления формы авторизации
    /// </summary>
    public class AuthViewModel : ReactiveObject, IActivatableViewModel
    {
        private readonly Dictionary<KeyValuePair<string, string>, Role> logins = new()
        {
            {
                new KeyValuePair<string, string>(
                    "Секретарь",
                    "30d7da7358bdd0034cfba938ae513e6ff40b9f46f9da771fae0409a6106eddcd"),
                Role.Secretary
            },
            {
                new KeyValuePair<string, string>(
                    "Завуч",
                    "e9cc897396aa8a157f14c410354312e1a7f44e579118234592f62c1a01b8935b"),
                Role.Chief
            },
            {
                new KeyValuePair<string, string>(
                    "Учитель",
                    "bb09cc6565896bb41e09532d8543c2c3f1341ab8edab7e59b0ae0c78fad46a9e"),
                Role.Teacher
            },
            {
                new KeyValuePair<string, string>(
                    "Классный руководитель",
                    "39302627e6b0696f855f60014d3ffeb04a39ac2e77ea06d2b1146db7268b857a"),
                Role.FormMaster
            }
        };

        public AuthViewModel()
        {
            Auth = ReactiveCommand.Create<Unit, Optional<Role>>(_auth);
            Username = "Секретарь";
            Password = "NVvkX7PH";
        }

        public string Username { get; set; }
        public string Password { get; set; }

        public ReactiveCommand<Unit, Optional<Role>> Auth { get; }
        public ViewModelActivator Activator { get; } = new();

        /// <summary>
        /// Вход
        /// </summary>
        /// <param name="u"></param>
        /// <returns>Роль</returns>
        private Optional<Role> _auth(Unit u)
        {
            var result = logins.TryGetValue(
                new KeyValuePair<string, string>(Username, Utils.SHA256Hash(Password)),
                out var rl);


            if (!result)
            {
                Utils.ShowError("Логин или пароль неверны");
                return Optional<Role>.None;
            }

            return Optional<Role>.Create(rl);
        }
    }
}