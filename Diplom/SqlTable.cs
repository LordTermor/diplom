﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Primitives;
using Avalonia.Controls.Templates;
using Avalonia.Data;
using Avalonia.Input;
using Avalonia.Layout;
using Avalonia.Media;
using Avalonia.ReactiveUI;
using DiplomModel;
using DynamicData.Binding;
using ReactiveUI;

namespace Diplom
{
    public class SqlTable<T> : ReactiveUserControl<SqlViewModel<T>> where T : EntityObject, new()
    {
        private Button _addButton;
        private Button _deleteButton;
        private DataGrid _grid;

        private KeyBinding _saveBinding;
        private Button _saveButton;
        private Button _refreshButton;

        public SqlTable()
        {
            Tag = typeof(T).Name;

            ViewModel = new SqlViewModel<T>();

            CreateUi();

            this.WhenActivated(disp =>
            {
                this.Bind(ViewModel,
                        model => model.Entities,
                        table => table._grid.Items)
                    .DisposeWith(disp);

                _addButton.Command = ReactiveCommand.Create(_showAddDialog).DisposeWith(disp);

                this.BindCommand(ViewModel,
                        model => model.Delete,
                        table => table._deleteButton,
                        _grid.GetObservable(DataGrid.SelectedIndexProperty))
                    .DisposeWith(disp);

                this.BindCommand(ViewModel,
                        model => model.Save,
                        table => table._saveBinding.Command)
                    .DisposeWith(disp);

                this.BindCommand(ViewModel,
                        model => model.Save,
                        table => table._saveButton)
                    .DisposeWith(disp);

                this.BindCommand(ViewModel,
                        model => model.Refresh,
                        table => table._refreshButton)
                    .DisposeWith(disp);

                ViewModel.Error.Subscribe(Utils.ShowError).DisposeWith(disp);
            });
        }

        private void CreateUi()
        {
            Content = new Grid
            {
                ColumnDefinitions = ColumnDefinitions.Parse("*"),
                RowDefinitions = RowDefinitions.Parse("*,Auto")
            };

            var content = (Grid) Content;


            _grid = new DataGrid
            {
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto
            };


            ViewModel.Properties.ForEach(
                column => { _grid.Columns.Add(ViewModel.ViewModelColumnToViewColumn(column)); });

            _addButton = new Button
            {
                Content = "Добавить"
            };

            _deleteButton = new Button
            {
                Content = "Удалить выбранное"
            };

            _refreshButton = new Button()
            {
                Content = "Обновить"
            };

            _saveButton = new Button {Content = "Сохранить", HorizontalAlignment = HorizontalAlignment.Stretch};


            var source = new Subject<int>();
            _grid[!DataGrid.SelectedIndexProperty] = source.ToBinding();


            _saveBinding = new KeyBinding {Gesture = new KeyGesture(Key.S, KeyModifiers.Control)};


            content.Children.Add(_grid);
            Grid.SetColumn(_grid, 0);
            Grid.SetColumnSpan(_grid, 4);
            Grid.SetRow(_grid, 0);

            var border = new Border {Padding = new Thickness(10)};

            var controlPanelGrid = new Grid {ColumnDefinitions = ColumnDefinitions.Parse("Auto,Auto,*,Auto")};

            border.Child = controlPanelGrid;


            controlPanelGrid.Children.Add(_addButton);
            Grid.SetColumn(_addButton, 0);
            Grid.SetRow(_addButton, 0);

            controlPanelGrid.Children.Add(_deleteButton);
            Grid.SetColumn(_deleteButton, 1);
            Grid.SetRow(_deleteButton, 0);

            controlPanelGrid.Children.Add(_refreshButton);
            Grid.SetColumn(_refreshButton, 2);
            Grid.SetRow(_refreshButton, 0);


            controlPanelGrid.Children.Add(_saveButton);
            Grid.SetColumn(_saveButton, 3);
            Grid.SetRow(_saveButton, 0);


            content.Children.Add(border);
            Grid.SetRow(border, 1);
            Grid.SetColumn(border, 0);
        }


        private async void _showAddDialog()
        {
            var dialog = new SqlAddDialog<T>(ViewModel.Properties)
            {
                Title = $"Добавить {Utils.DisplayCamelCaseString(typeof(T).Name)}"
            };
            var toAdd = await dialog.ShowDialog<T>(
                ((IClassicDesktopStyleApplicationLifetime) Application.Current.ApplicationLifetime).MainWindow);
            if (toAdd != null) await ViewModel.Add.Execute(toAdd);
        }
    }
}